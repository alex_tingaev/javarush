package com.javarush.test.level21.lesson08.task03;

/* Запретить клонирование
Разрешите клонировать класс А
Запретите клонировать класс B
Разрешите клонировать класс C
Метод main не участвует в тестировании.
*/
public class Solution {
    public static class A implements Cloneable {
        private int i;
        private int j;

        public A(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        protected A clone() throws CloneNotSupportedException
        {

            if(!(this.getClass().isInstance(B.class))) return new A(getI(), getJ());
            else throw new CloneNotSupportedException();
        }
    }

    public static class B extends A {
        private String name;

        public B(int i, int j, String name) {
            super(i, j);
            this.name = name;
        }


        public String getName() {
            return name;
        }

        @Override
        public B clone() throws CloneNotSupportedException
        {
            throw new CloneNotSupportedException("ploho!");
        }
    }

    public static class C extends B  {
        public C(int i, int j, String name) {
            super(i, j, name);
        }

        @Override
        public C clone() throws CloneNotSupportedException
        {
            return new C(getI(), getJ(), getName());
        }

    }



    public static void main(String[] args)
    {
        A a = new A(1, 1);
        B b = new B(2, 2, "class B");
        C c = new C(3, 3, "class C");

        A clone = null;
        try
        {
            clone = a.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        System.out.println(clone.getI());
        System.out.println(clone.getJ());

        try
        {
            clone = b.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        System.out.println(clone.getI());
        System.out.println(clone.getJ());

        try
        {
            clone = c.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        System.out.println(clone.getI());
        System.out.println(clone.getJ());
    }

}
