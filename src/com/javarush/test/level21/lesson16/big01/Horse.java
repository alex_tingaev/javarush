package com.javarush.test.level21.lesson16.big01;

/**
 * artes on 15.07.16. tingaev@gmail.com
 **/
public class Horse
{
    private String name;
    private double speed;
    private  double distance;

    public Horse(String name, double speed, double distance)
    {
        this.name = name;
        this.speed = speed;
        this.distance = distance;
    }

    public void move() {
        distance += speed * Math.random();
    }
    public void print(){
        String route = "";
        for (int i = 0; i < distance; i++) {
            route = route + ".";
        }
        route = route + name;
        System.out.println(route);
    }

    public String getName()
    {
        return name;
    }

    public double getSpeed()
    {
        return speed;
    }

    public double getDistance()
    {
        return distance;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setSpeed(double speed)
    {
        this.speed = speed;
    }

    public void setDistance(double distance)
    {
        this.distance = distance;
    }

    public Horse() {}
}
