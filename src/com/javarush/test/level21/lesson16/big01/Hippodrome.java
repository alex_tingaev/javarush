package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

/**
 * artes on 15.07.16. tingaev@gmail.com
 **/
public class Hippodrome
{
    public ArrayList<Horse> horses = new ArrayList<Horse>();
    public static Hippodrome game;

    public ArrayList<Horse> getHorses()
    {
        return horses;
    }


    public void move()
    {
        for (Horse horse : game.horses) {
            horse.move();
        }
    }
    public void print()
    {
        for (Horse horse : game.horses) {
            horse.print();
            System.out.println();
            System.out.println();
        }
    }
    public void run() throws InterruptedException
    {
        for (int i = 0; i < 100; i++) {
            move();
            print();
            Thread.sleep(200);
        }
    }
    public Horse getWinner(){
        Horse winner = new Horse("NotAWinner", 0, 0);
        for (Horse horse : getHorses()){
            if (horse.getDistance() > winner.getDistance()) winner = horse;
        }
        return winner;
    }
    public void printWinner(){
        System.out.println("Winner is " + getWinner().getName() + "!");
    }

    public Hippodrome() {}

    public static void main(String[] args) throws InterruptedException
    {
        game = new Hippodrome();

        game.getHorses().add(new Horse("Putin", 3, 0));
        game.getHorses().add(new Horse("Medvedev", 3, 0));
        game.getHorses().add(new Horse("Navalny", 3, 0));

        game.run();
        game.printWinner();


    }
}
