package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        // напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        reader.close();
        Scanner scanner = new Scanner(new FileInputStream(file));
        ArrayList<Integer> arrayList = new ArrayList<>();

        while (scanner.hasNext()) {
            int buffer = 0;
            buffer = scanner.nextInt();
            if (buffer % 2 == 0) arrayList.add(buffer);
        }
        scanner.close();
        Collections.sort(arrayList);

        for (int i : arrayList) {
            System.out.println(i);
        }
    }
}
