package com.javarush.test.level26.lesson15.big01;

import java.util.HashMap;
import java.util.Map;

/**
 * artes on 27/10/16 tingaev@gmail.com
 **/
public class CurrencyManipulator
{
    String currencyCode;
    Map <Integer, Integer> denominations;

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination))
            denominations.put(denomination, denominations.get(denomination) + count);
        else
        denominations.put(denomination, count);
    }

    public int getTotalAmount() {
        CurrencyManipulator manipulator = this;

        int result = 0;

        for (Map.Entry entry : manipulator.denominations.entrySet()) {
            result += (int) entry.getKey() * (int) entry.getValue();
        }

        return result;
    }



    public CurrencyManipulator(String currencyCode)
    {
        this.currencyCode = currencyCode;
        denominations = new HashMap<>();
    }
}
