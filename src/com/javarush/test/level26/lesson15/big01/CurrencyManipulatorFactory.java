package com.javarush.test.level26.lesson15.big01;

import java.util.HashMap;

/**
 * artes on 27/10/16 tingaev@gmail.com
 **/
public abstract class CurrencyManipulatorFactory
{
    public static final HashMap<String, CurrencyManipulator> manipulators = new HashMap<String, CurrencyManipulator>();


    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode) {
        CurrencyManipulator manipulator = manipulators.get(currencyCode);

        if (manipulator == null) {
            manipulator = new CurrencyManipulator(currencyCode);
            manipulators.put(currencyCode, manipulator);
            return manipulator;
        }
        else return manipulator;
    }
}
