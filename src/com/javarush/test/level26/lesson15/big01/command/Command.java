package com.javarush.test.level26.lesson15.big01.command;

/**
 * artes on 1/11/16 tingaev@gmail.com
 **/
interface Command
{
void execute();
}
