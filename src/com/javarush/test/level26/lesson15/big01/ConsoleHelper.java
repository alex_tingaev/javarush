package com.javarush.test.level26.lesson15.big01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * artes on 25/10/16 tingaev@gmail.com
 **/
public class ConsoleHelper
{
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    public static void writeMessage(String message)
    {
        System.out.println(message);
    }

    public static String readString()
    {
        String s = "";
        try
        {
            s = reader.readLine();
        }
        catch (Exception e)
        {
        }

        return s;
    }

    public static String askCurrencyCode()
    {
        boolean isValid = false;
        String code = "";

        while (!isValid)
        {
            writeMessage("Введите код валюты: ");
            try
            {
                code = reader.readLine();

                if (code.trim().toCharArray().length == 3)
                {
                    isValid = true;
                } else
                {
                    writeMessage("Данные некорректны, повторите");
                }

            }
            catch (IOException e)
            {
                writeMessage("Данные некорректны, повторите");
            }
        }
        return code.toUpperCase();
    }

    public static String[] getValidTwoDigits(String currencyCode)
    {
        String[] result = new String[2];
        boolean isValid = false;

        while (!isValid)
        {
            writeMessage("Введите номинал и количество валюты через пробел, например 100 15");
            try
            {
                result = reader.readLine().split(" ");
                Integer.parseInt(result[1]);
            }
            catch (Exception e)
            {
                writeMessage("Неверные данные");
            }

            if (Integer.parseInt(result[1]) > 0)
                isValid = true;
            else writeMessage("Неверные данные");

        }
        return result;
    }

    public static Operation askOperation() {

        System.out.println("Пожалуйста, введите номер операции для выполнения: ");
        System.out.println("   1 - Получить информацию о балансе;");
        System.out.println("   2 - Внести наличные;");
        System.out.println("   3 - Снять наличные;");
        System.out.println("   4 - Вернуть карту и выйти из системы.");

        Operation operation = null;

            try {
                operation = Operation.getAllowableOperationByOrdinal(Integer.parseInt(readString()));
            }
            catch (Exception e) {
                System.out.println("Неверные данные, пожалуйста, повторите ввод.");
                operation = askOperation();
            }

        return operation;
    }

}
