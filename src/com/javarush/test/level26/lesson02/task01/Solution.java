package com.javarush.test.level26.lesson02.task01;

import java.util.Arrays;
import java.util.Comparator;

/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/
public class Solution {
    public static Integer[] sort(Integer[] array) {
        Arrays.sort(array);
        final double median;
        if (array.length % 2 == 0)
            median = ((double)array[array.length/2] + (double)array[array.length/2 - 1])/2;
        else
            median = (double) array[array.length/2];

        Comparator<Integer> compareByMedian = new Comparator<Integer>()
        {
            @Override
            public int compare(Integer o1, Integer o2)
            {
                if ( Math.abs(median - o1) == Math.abs(median - o2))
                    return o1.compareTo(o2);
                else
                    return (int) ((Math.abs(median - o1)) - Math.abs(median - o2));
            }
        };

        Arrays.sort(array, compareByMedian);
        return array;

    }


    public static void main(String[] args)
    {
        Integer[] array = new Integer[10];
        array[0] = 1;
        array[1] = 3;
        array[2] = 3;
        array[3] = 3;
        array[4] = 4;
        array[5] = 12;
        array[6] = 13;
        array[7] = 13;
        array[8] = 18;
        array[9] = 32;

        sort(array);

        for (Integer integer : array)
        {
            System.out.println(integer);
        }
    }

}
