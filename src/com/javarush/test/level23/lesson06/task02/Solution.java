package com.javarush.test.level23.lesson06.task02;

/* Рефакторинг
Отрефакторите класс Solution: вынесите все константы в public вложенный(nested) класс Constants.
Запретите наследоваться от Constants.
*/
public class Solution {

    public final static class Constants
    {
        final static String serverNotAccessibleException = "Server is not accessible for now.";
        final static String unauthorizedUserException = "User is not authorized.";
        final static String bannedUserException = "User is banned.";
        final static String restrictionException = "Access is denied.";
    }

        public class ServerNotAccessibleException extends Exception
        {
            public ServerNotAccessibleException()
            {
                super(Constants.serverNotAccessibleException);
            }

            public ServerNotAccessibleException(Throwable cause)
            {
                super(Constants.serverNotAccessibleException, cause);
            }
        }

        public class UnauthorizedUserException extends Exception
        {
            public UnauthorizedUserException()
            {
                super(Constants.unauthorizedUserException);
            }

            public UnauthorizedUserException(Throwable cause)
            {
                super(Constants.unauthorizedUserException, cause);
            }
        }

        public class BannedUserException extends Exception
        {
            public BannedUserException()
            {
                super(Constants.bannedUserException);
            }

            public BannedUserException(Throwable cause)
            {
                super(Constants.bannedUserException, cause);
            }
        }

        public class RestrictionException extends Exception
        {
            public RestrictionException()
            {
                super(Constants.restrictionException);
            }

            public RestrictionException(Throwable cause)
            {
                super(Constants.restrictionException, cause);
            }
        }
}
