package com.javarush.test.level22.lesson05.task01;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
Метод main не участвует в тестировании.
*/
public class Solution {
    public static String getPartOfString(String string) throws TooShortStringException
    {
        if (string == null) throw new TooShortStringException();

        String [] buffer = string.split(" ");
        if (buffer.length < 6) throw new TooShortStringException();
        else return String.format("%s %s %s %s", buffer[1], buffer[2], buffer[3], buffer[4]);
    }

    public static class TooShortStringException extends Throwable {

    }

    public static void main(String[] args) throws TooShortStringException
    {
        String s = "";
        String result = getPartOfString(s);
        System.out.println(result);
    }
}
