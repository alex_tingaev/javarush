package com.javarush.test.level22.lesson09.task02;

import java.util.HashMap;
import java.util.Map;

/* Формируем Where
Сформируйте часть запроса WHERE используя StringBuilder.
Если значение null, то параметр не должен попадать в запрос.
Пример:
{"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null}
Результат:
"name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'"
*/
public class Solution {

    public static StringBuilder getCondition(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        String key;
        String value;
        if (params.isEmpty()) return sb;
        else
        for (Map.Entry entry : params.entrySet()) {
            key = (String) entry.getKey();
            value = (String) entry.getValue();
            if (key!= null && value != null && !key.contains("null") && !value.contains("null")) {
                sb.append(String.format("%s = '%s' and ", key, value));
            }
        }
        sb.delete(sb.length()-5, sb.length());
        return sb;
    }

    public static void main(String[] args)
    {
        Map <String, String> map = new HashMap<>();
        map.put("name", "Ivanov");
        map.put("country", "Ukraine");
        map.put("city", "Kiev");
        map.put("age", null);
        map.put(null, null);
        map.put(null, null);
        map.put(null, null);
        System.out.println(getCondition(map));

    }
}
