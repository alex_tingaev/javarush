package com.javarush.test.level22.lesson09.task01;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример содержимого файла
рот тор торт о
о тот тот тот
Вывод:
рот тор
о о
тот тот
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(new File(reader.readLine()));
        ArrayList<String> list = new ArrayList<>();
        StringBuilder sb;
        String word1;
        String word2;
        Pair pair;
        int count = 0;

        while (scanner.hasNext()) {
            list.add(scanner.next());
        }
        reader.close();
        scanner.close();

        for (int i = 0; i < list.size()-1; ) {
            word1 = list.get(i);
            System.out.println(word1);
            for (int j = 1; i < list.size()-1; ) {
                word2 = list.get(j);
                System.out.println(word2);
                sb = new StringBuilder(word2).reverse();
                if (word1.equals(sb.toString()) && !word2.equals(sb.toString()))
                {
                    pair = new Pair(word1, word2);
                    result.add(count, pair);
                    count++;
                }
                else j--;
                }
                i++;
            }

        for (Pair p : result) {
            System.out.println(p.toString());
        }

    }

    public static class Pair {
        String first;
        String second;

        public Pair(String word1, String word2)
        {
            this.first = word1;
            this.second = word2;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;
        }
    }

}
