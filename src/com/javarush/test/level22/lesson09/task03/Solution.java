package com.javarush.test.level22.lesson09.task03;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //...
        ArrayList<String> word = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext())
        {
            word.add(scanner.next());
        }
        Collections.shuffle(word);

        String[] words = new String[word.size()];
        words = word.toArray(words);

        StringBuilder result = getLine(words);
        System.out.println(result.toString());
    }

    public static StringBuilder getLine(String... words) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        if (words == null || (words.length == 0)) return sb;

        ArrayList<String> listWithWords = new ArrayList<String>();
        Collections.addAll(listWithWords, words);

        Boolean sequenceIsFind = false;
        int countOfSequence = 0;

        while (!sequenceIsFind)
        {
            for (int i = 0; i < listWithWords.size() - 1; )
            {
                if (isCharSame(listWithWords.get(i), listWithWords.get(i + 1)))
                {
                    countOfSequence++;
                    i++;
                } else
                {
                    countOfSequence = 0;
                    break;
                }
            }

            if (countOfSequence == listWithWords.size() - 1) sequenceIsFind = true;
            else Collections.shuffle(listWithWords);
        }

        for (String s : listWithWords)
        {
            sb.append(s).append(" ");
        }

        sb.deleteCharAt(sb.length() - 1);

        return sb;
    }

    public static boolean isCharSame(String s1, String s2)
    {
        return s1.substring(s1.length() - 1, s1.length()).equalsIgnoreCase(s2.substring(0, 1));
    }
}
