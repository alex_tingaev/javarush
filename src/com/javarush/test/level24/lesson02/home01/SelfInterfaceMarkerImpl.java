package com.javarush.test.level24.lesson02.home01;

/**
 * artes on 1/08/16. tingaev@gmail.com
 **/
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{
    public SelfInterfaceMarkerImpl()
    {
    }
    public void method1(){
        System.out.println("this is method1");
    }
    public void method2(){
        System.out.println("this is method2");
    }
}
