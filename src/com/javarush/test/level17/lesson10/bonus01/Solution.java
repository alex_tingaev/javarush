package com.javarush.test.level17.lesson10.bonus01;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD
CrUD - Create, Update, Delete
Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-c  - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u  - обновляет данные человека с данным id
-d  - производит логическое удаление человека с id
-i  - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)

id соответствует индексу в списке
Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat

Пример параметров: -c Миронов м 15/04/1990
*/

public class Solution
{
    public static List<Person> allPeople = new ArrayList<Person>();

    static
    {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws IOException, ParseException, NullPointerException
    {
        //start here - начни тут
        SimpleDateFormat dateInputFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat dateOutputFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

        Person person;

        if (args.length > 0)
        {
            switch (args[0])
            {
                case "-c":

                    if (args[2].equals("м"))
                    {
                        allPeople.add(person = Person.createMale(args[1], dateInputFormatter.parse(args[3])));
                        System.out.println(allPeople.indexOf(person));
                        break;

                    } else if (args[2].equals("ж"))
                    {
                        allPeople.add(person = Person.createFemale(args[1], dateInputFormatter.parse(args[3])));
                        System.out.println(allPeople.indexOf(person));
                        break;
                    }

                case "-u":
                {
                    person = allPeople.get(Integer.parseInt(args[1]));
                    person.setName(args[2]);
                    person.setSex(args[3].equals("м") ? Sex.MALE : Sex.FEMALE);
                    person.setBirthDay(dateInputFormatter.parse(args[4]));
                    break;
                }

                case "-d":
                {
                    person = allPeople.get(Integer.parseInt(args[1]));

                    person.setName(null);
                    person.setSex(null);
                    person.setBirthDay(null);
                    break;
                }

                case "-i":
                {
                    person = allPeople.get(Integer.parseInt(args[1]));
                    System.out.println(person.getName() + " " + (person.getSex().equals(Sex.MALE) ? "м" : "ж") + " " + dateOutputFormatter.format(person.getBirthDay()));
                    break;
                }
            }
        }
    }
}
