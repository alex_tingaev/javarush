package com.javarush.test.level25.lesson07.home01;

public class TaskManipulator implements Runnable, CustomThreadManipulator
{
    Thread myThread;

    @Override
    public void run()
    {
        try
        {
            while (!myThread.isInterrupted())
            {
                Thread.sleep(0);
                System.out.println(myThread.getName());
                Thread.sleep(90);
            }
        }
        catch (InterruptedException e)
        {
        }
    }

    @Override
    public void start(String threadName)
    {
        myThread = new Thread(this);
        myThread.setName(threadName);
        myThread.start();
    }

    @Override
    public void stop()
    {
        myThread.interrupt();
    }
}
