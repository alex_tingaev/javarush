package com.javarush.test.level25.lesson05.home01;

/**
 * artes on 8/09/16 tingaev@gmail.com
 **/
public class LoggingStateThread extends Thread
{
    Thread targetThread;


    public LoggingStateThread(Thread targetThread)
    {
        this.targetThread = targetThread;
        setDaemon(true);
    }

    @Override
    public void run()
    {
        Thread.State state = targetThread.getState();
        System.out.println(state);

        while (state != State.TERMINATED)
        {
            if (state != targetThread.getState())
            {
                state = targetThread.getState();
                System.out.println(state);
            }
        }

    }
}

