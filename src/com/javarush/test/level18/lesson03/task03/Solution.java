package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байт или байты с максимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Date startTime = new Date();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());

        Map<Integer, Integer> mapOfBytes = new HashMap<>();

        while (inputStream.available() > 0) {
            int value = inputStream.read();

            if (!mapOfBytes.containsKey(value))
                mapOfBytes.put(value, 1);

            else mapOfBytes.put(value, mapOfBytes.get(value) + 1);
        }

        reader.close();
        inputStream.close();

        int maxValueOfMap = Collections.max(mapOfBytes.values());

        System.out.println("вот список всех встречающихся байт:");
        for (Map.Entry entry : mapOfBytes.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println();

        for (Map.Entry entry : mapOfBytes.entrySet()) {
            if (entry.getValue().equals(maxValueOfMap)) System.out.print("наиболее часто встречающийся байт - " + entry.getKey() + " ");
        }
        Date finishTime = new Date();


        long timeAtWork = finishTime.getTime() - startTime.getTime();
        System.out.println();
        System.out.println();

        System.out.print("время работы программы - " + timeAtWork + "мс");

        }
    }

