package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
       
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());

        Map<Integer, Integer> mapOfBytes = new HashMap<>();

        while (inputStream.available() > 0) {
            int value = inputStream.read();

            if (!mapOfBytes.containsKey(value))
                mapOfBytes.put(value, 1);

            else mapOfBytes.put(value, mapOfBytes.get(value) + 1);
        }

        reader.close();
        inputStream.close();

        int minValueOfMap = Collections.min(mapOfBytes.values());
        
        for (Map.Entry entry : mapOfBytes.entrySet()) {
            if (entry.getValue().equals(minValueOfMap)) System.out.print(entry.getKey());
        }
        
    }
    }
