package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;




/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());

        Map<Integer, Integer> map = new TreeMap<>();

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (map.containsKey(data)) continue;
            else map.put(data, 1);
        }

        reader.close();
        inputStream.close();

        for (Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey());
        }
    }
}
