package com.javarush.test.level18.lesson10.bonus01;

/* Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        String fileName = "";
        String fileOutputNAme = "";

        final int key = 1;

        File inputFile;
        File outputFile;

        FileInputStream fis;
        BufferedInputStream bufferedReader;

        FileOutputStream fos;
        BufferedOutputStream bufferedOutputStream;

        switch (args[0]) {
            case "-e" : {
                fileName = args[1];
                fileOutputNAme = args[2];

                inputFile = new File(fileName);
                outputFile = new File(fileOutputNAme);
                if (!outputFile.exists()) outputFile.createNewFile();

                fis = new FileInputStream(inputFile);
                bufferedReader = new BufferedInputStream(fis);

                fos = new FileOutputStream(fileOutputNAme);
                bufferedOutputStream = new BufferedOutputStream(fos);

                while (bufferedReader.available() > 0) {
                    int buffer = bufferedReader.read();
                    int encryptedBuffer = buffer+key;
                    bufferedOutputStream.write(encryptedBuffer);
                }

                bufferedOutputStream.close();
                bufferedReader.close();

                break;}

            case "-d" : {
                fileName = args[1];
                fileOutputNAme = args[2];

                inputFile = new File(fileName);
                outputFile = new File(fileOutputNAme);
                if (!outputFile.exists()) outputFile.createNewFile();

                fis = new FileInputStream(inputFile);
                bufferedReader = new BufferedInputStream(fis);

                fos = new FileOutputStream(fileOutputNAme);
                bufferedOutputStream = new BufferedOutputStream(fos);

                while (bufferedReader.available() > 0) {
                    int buffer = bufferedReader.read();
                    int encryptedBuffer = buffer-key;
                    bufferedOutputStream.write(encryptedBuffer);
                }

                bufferedOutputStream.close();
                bufferedReader.close();

                break;}
        }

    }

}
