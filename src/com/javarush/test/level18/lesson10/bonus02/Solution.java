package com.javarush.test.level18.lesson10.bonus02;

/* Прайсы
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается со следующим набором параметров:
-c productName price quantity
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-c  - добавляет товар с заданными параметрами в конец файла, генерирует id самостоятельно, инкрементируя максимальный id, найденный в файле

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/


import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        File myFile = getAddressOfFile();
        int ID = getTheNextIDFromFile(myFile);
        String productName = args[1];
        double price = Double.parseDouble(args[2]);
        int quantity = Integer.parseInt(args[3]);

        appendDataToTheFile(myFile, ID, productName, price, quantity);
    }

    public static File getAddressOfFile() throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File myFile = new File(reader.readLine());
        reader.close();

        return myFile;
    }

    public static Integer getTheNextIDFromFile(File file) throws FileNotFoundException
    {
        String line = "";
        ArrayList<Integer> listOfID = new ArrayList<>();

        try (Scanner s = new Scanner(file).useDelimiter("\\n"))
        {
            while (s.hasNext())
            {
                line = s.next();
                try
                {
                    listOfID.add(Integer.parseInt(line.substring(0, 8).trim()));
                }
                catch (Exception e) {
                    listOfID.add(0);
                }
            }
            s.close();
            listOfID.add(0);
        }
        return Collections.max(listOfID) == 99999999 ? Collections.max(listOfID) : Collections.max(listOfID) + 1;
    }

    public static void appendDataToTheFile(File file, int ID, String productName, double price, int quantity) throws IOException
    {
        FileWriter fileWriter = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter out = new PrintWriter(bufferedWriter);

        out.printf("%-4d%-30s%-8.2f%-4d", ID, productName, price, quantity);
        out.close();
    }
}