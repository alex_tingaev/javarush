package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки. Не использовать try-with-resources

Пример вывода:
, 19
- 7
f 361
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        FileInputStream fis = new FileInputStream(args[0]);
        Map<Character, Integer> charMap = new TreeMap<>();

        while (fis.available() > 0)
        {
            char buffer = (char) fis.read();
            if (!charMap.containsKey(buffer)) charMap.put(buffer, 1);
            else charMap.put(buffer, charMap.get(buffer) + 1);
        }
        fis.close();

        for (Map.Entry entry : charMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}