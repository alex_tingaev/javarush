package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        File myFile = getAddressOfFile();
        ArrayList<String> buffer = getAllDataFromFile(myFile);
        String argument = args[0];
        String ID = args[1];
        String productName;
        double price;
        int quantity;

        switch (argument)
        {
            case "-u":
                productName = args[2];
                price = Double.parseDouble(args[3]);
                quantity = Integer.parseInt(args[4]);

                updateStringInBuffer(buffer, ID, productName, price, quantity);
                applyChangesForFile(myFile, buffer);
                break;

            case "-d":
                deleteStringInBuffer(buffer, ID);
                applyChangesForFile(myFile, buffer);
                break;
        }
    }
    public static File getAddressOfFile() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File myFile = new File(reader.readLine());
        reader.close();

        return myFile;
    }

    public static ArrayList getAllDataFromFile(File file) throws FileNotFoundException {
        ArrayList<String> buffer = new ArrayList<String>();
        String line;

        try (Scanner s = new Scanner(file).useDelimiter("\\n"))
        {
            while (s.hasNext())
            {
                line = s.next();
                buffer.add(line);
                }
            
            s.close();
        }
        return buffer;
    }

    public static void updateStringInBuffer(ArrayList<String> buffer, String ID, String productName, double price, int quantity) {
        String updatedString = String.format("%-8s%-30s%-8.2f%-4d", ID, productName, price, quantity);

        for (int i = 0; i < buffer.size(); i++)
        {
            try
            {
                if (buffer.get(i).substring(0, 8).trim().equals(ID)) buffer.set(i, updatedString);
            }
            catch (Exception e) {
                continue;
            }
        }
    }

    public static void deleteStringInBuffer(ArrayList<String> buffer, String ID) {
        for (int i = 0; i < buffer.size(); i++)
        {
            try
            {
                if (buffer.get(i).substring(0, 8).trim().equals(ID))
                {
                    buffer.remove(i);
                    i--;
                }
            }
            catch (Exception e) {
                continue;
            }
        }
    }

    public static void applyChangesForFile(File file, ArrayList<String> buffer) throws IOException {
        FileWriter fileWriter = new FileWriter(file, false);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter out = new PrintWriter(bufferedWriter);

        for (String s : buffer)
        {
            out.println(s);
        }
        out.close();
    }
}
