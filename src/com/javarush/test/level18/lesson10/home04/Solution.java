package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        reader.close();

        FileInputStream is1 = new FileInputStream(file1);
        byte[] buffer1 = new byte[is1.available()];
        while (is1.available() > 0) {
            is1.read(buffer1);
        }
        is1.close();


        FileInputStream is2 = new FileInputStream(file2);
        FileOutputStream os2 = new FileOutputStream(file1);
        while (is2.available() > 0) {
            int data = is2.read();
            os2.write(data);
        }
        is2.close();
         for (byte b : buffer1) {
             os2.write(b);
         }

        os2.close();
    }
}
