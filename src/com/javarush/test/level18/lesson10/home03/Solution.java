package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать в первый файл содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String addressOfFile1 = reader.readLine();
        String addressOfFile2 = reader.readLine();
        String addressOfFile3 = reader.readLine();

        reader.close();

        FileInputStream fileInputStream3 = new FileInputStream(addressOfFile3);
        FileInputStream fileInputStream2 = new FileInputStream(addressOfFile2);
        FileOutputStream fileOutputStream = new FileOutputStream(addressOfFile1);

        byte[] buffer2 = new byte[1000];
        int countOfFile2 = 0;

        while (fileInputStream2.available() > 0) {
            countOfFile2 = fileInputStream2.read(buffer2);
            fileOutputStream.write(buffer2, 0, countOfFile2);
        }

        byte[] buffer3 = new byte[1000];
        int countOfFile3;
        
        while (fileInputStream3.available() > 0) {
            countOfFile3 = fileInputStream3.read(buffer3);
            fileOutputStream.write(buffer3, 0, countOfFile3);
        }

        fileInputStream2.close();
        fileInputStream3.close();
        fileOutputStream.close();
    }
}
