package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution
{
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = "";

        while (true)
        {
            fileName = reader.readLine();
            if (fileName.isEmpty() || fileName.contains("exit"))
            {
                break;
            }

            new ReadThread(fileName).start();
        }
        reader.close();
    }

    public static class ReadThread extends Thread
    {

        private String fileName;

        public ReadThread(String fileName) throws IOException
        {
            this.fileName = fileName;
        }

        @Override
        public void run()
        {
            Map<Integer, Integer> mapOfBytes = new HashMap<>();

                FileInputStream fis = null;
                try
                {
                    fis = new FileInputStream(fileName);
                }
                catch (FileNotFoundException e)
                {
                }

                try
                {
                    while (fis.available() > 0)
                    {
                        int buffer = fis.read();
                        if (mapOfBytes.containsKey(buffer)) mapOfBytes.put(buffer, mapOfBytes.get(buffer) + 1);
                        else mapOfBytes.put(buffer, 1);
                    }
                    fis.close();

                    int maxByte = 0;
                    int buffer = 0;
                    int keyOfMaxByte= 0;

                    for (Map.Entry entry : mapOfBytes.entrySet())
                    {
                        buffer = (int) entry.getValue();
                        if (buffer >= maxByte)
                            {maxByte = buffer;
                            keyOfMaxByte = (int) entry.getKey();}
                    }

                     resultMap.put(fileName, keyOfMaxByte);

                }
                catch (IOException e)
                {
                }
            }
        }
    }

