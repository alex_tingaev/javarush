package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        String fileName = null;
        BufferedReader fileNameReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedInputStream bis;
        FileOutputStream bos;

        ArrayList<File> fileNames = new ArrayList<File>();

        while (true)
        {
            fileName = fileNameReader.readLine();
            if (fileName.isEmpty() || fileName.contains("end")) break;
            File file = new File(fileName);
            fileNames.add(file);
        }
        fileNameReader.close();

        Collections.sort(fileNames);

        String basename = fileNames.get(0).getName();
        String[] split = basename.split("\\.");

        StringBuilder builder = new StringBuilder();
        builder.append(split[0]).append(".").append(split[1]);
        basename = builder.toString();

        File finalFile = new File(fileNames.get(0).getParent() + "/" + basename);
        finalFile.createNewFile();

        bos = new FileOutputStream(finalFile);

        for (int i = 0; i < fileNames.size(); i++)
        {
            bis = new BufferedInputStream(new FileInputStream(fileNames.get(i)));

            while (bis.available() > 0)
            {
                int buffer = bis.read();
                bos.write(buffer);
            }
            if (i == fileNames.size()-1) bis.close();
        }
        bos.close();
    }
}

