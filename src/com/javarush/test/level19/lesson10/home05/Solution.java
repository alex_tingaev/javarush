package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит строки со слов, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws FileNotFoundException
    {

        File file1 = new File(args[0]);
        File file2 = new File(args[1]);

        Scanner scanner = new Scanner(new FileInputStream(file1));
        PrintWriter writer = new PrintWriter(new FileOutputStream(file2));

        String buffer = "";

        while (scanner.hasNext())
        {
            buffer = scanner.next();
            if (findDigit(buffer))
                writer.print(buffer + " ");
        }
        scanner.close();
        writer.close();
    }


    public static boolean findDigit(String s)
    {
        char[] buffer;
        buffer = s.toCharArray();

        for (char c : buffer)
        {
            if (Character.isDigit(c))
            {
                return true;
            }
        }
        return false;
    }


}






