package com.javarush.test.level19.lesson10.home03;

import java.io.File;
import java.io.IOException;
import java.util.*;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException {
        File myFile = new File(args[0]);
        readAllPeopleFromFile(myFile, PEOPLE);
    }

    private static void readAllPeopleFromFile(File myFile, List list) throws IOException
    {
        Person person;

        Scanner scanner = new Scanner(myFile);
        while (scanner.hasNext()){
            String s = scanner.nextLine();
            person = getNewPersonFromString(s);
            list.add(person);
        }
        scanner.close();
    }

    private static Person getNewPersonFromString(String s)
    {
        Date birthday;
        String name;
        int year;
        int month;
        int day;
        String buffer[] = s.split(" ");

        StringBuilder stringBuilder = new StringBuilder();
        for (String string : buffer){
            stringBuilder.append(string.replaceAll("[^A-Za-zА-Я-а-я]", ""));
            stringBuilder.append(" ");
        }
        name = stringBuilder.toString().trim();
        year = Integer.parseInt(buffer[buffer.length-1]);
        month = Integer.parseInt(buffer[buffer.length-2]);
        day = Integer.parseInt(buffer[buffer.length-3]);
        birthday = new Date(year-1900, month-1, day);
        return new Person(name, birthday);
    }

}
