package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки. Не использовать try-with-resources

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        File file1 = new File(args[0]);
        File file2 = new File(args[1]);

        WriteLongWords(file1, file2);
    }

    private static void WriteLongWords(File file1, File file2) throws IOException
    {
        Scanner scanner = new Scanner(file1);
        PrintWriter writer = new PrintWriter(file2);
        ArrayList<String> list = new ArrayList<>();

        while (scanner.hasNext())
        {
            String s = "";
            s = scanner.next();
            if (s.matches(".{7,}")) list.add(s + ",");
        }
        list.set(list.size() - 1, list.get(list.size() - 1).replace(",", ""));

        for (String string : list)
        {
            writer.print(string);
        }

        scanner.close();
        writer.close();
    }
}
