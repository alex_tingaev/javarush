package com.javarush.test.level19.lesson10.home04;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Ищем нужные строки
Считать с консоли имя файла.
Вывести в консоль все строки из файла, которые содержат всего 2 слова из списка words
Закрыть потоки. Не использовать try-with-resources

Пример: words содержит слова А, Б, В
Строки:
В Б А Д  //3 слова из words, не подходит
Д А Д    //1 слово из words, не подходит
Д А Б Д  //2 слова - подходит, выводим
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) throws IOException  {
        File file = getAdressOfFile();
        printStringFromFileWth2matchs(file, words);

    }

    private static void printStringFromFileWth2matchs(File file, List<String> words) throws IOException
    {
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            int count = 0;
            for (int i = 0; i < words.size(); i++) {
                if (s.contains(words.get(i))) count++;
            }
            if (count == 2) System.out.println(s);
        }
        scanner.close();
    }

    private static File getAdressOfFile() throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        reader.close();
        return file;
    }
}
