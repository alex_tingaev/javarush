package com.javarush.test.level19.lesson10.home08;

/* Перевертыши
1 Считать с консоли имя файла.
2 Для каждой строки в файле:
2.1 переставить все символы в обратном порядке
2.2 вывести на экран
3 Закрыть потоки. Не использовать try-with-resources

Пример тела входного файла:
я - программист.
Амиго

Пример результата:
.тсиммаргорп - я
огимА
*/

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        File file = new File(reader.readLine());
        reader.close();

        Scanner scanner = new Scanner(file);
        String s = "";
        StringBuilder stringBuilder;

        while (scanner.hasNext()) {
            s = scanner.nextLine();
            stringBuilder = new StringBuilder(s);
            s = stringBuilder.reverse().toString();

            System.out.println(s);
        }
        scanner.close();
    }
}
