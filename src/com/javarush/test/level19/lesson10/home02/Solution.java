package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/


import java.io.File;
import java.io.IOException;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        File file = new File(args[0]);
        Map<String, Double> mapOfEmployers = readEmployersFromFile(file);
        Map<String, Double> sortedMap = sortByValue(mapOfEmployers);

        printMaxSalaries(sortedMap);
    }

    private static void printMaxSalaries(Map<String, Double> sortedMap)
    {
        String employer = "";
        Double salary = 0.0;
        Boolean isCheck = false;

        for (Map.Entry entry : sortedMap.entrySet())
        {
            if((isCheck & !(salary.equals(entry.getValue())))) break;
            employer = String.valueOf(entry.getKey());
            salary = (Double) entry.getValue();
            System.out.println(employer);
            isCheck = true;
        }
        }


    private static Map<String, Double> readEmployersFromFile(File file) throws IOException
    {
        Map<String, Double> map = new HashMap<>();

        String[] buffer;

        Scanner scanner = new Scanner(file);
        while (scanner.hasNext())
        {
            buffer = scanner.nextLine().split(" ");
            if (!(map.containsKey(buffer[0]))) map.put(buffer[0], Double.parseDouble(buffer[1]));
            else map.put(buffer[0], (Double.parseDouble(buffer[1]) + map.get(buffer[0])));
        }
        scanner.close();
        return map;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue(Map<K, V> map)
    {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
