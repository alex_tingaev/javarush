package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        File myFile = getAddressOfFile();
        System.out.println(getCountOfWord(myFile));

    }

    private static int getCountOfWord(File file) throws IOException
    {
        int result = 0;

        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("[^a-zA-Z]");

        while (scanner.hasNext())
        {
            String word = scanner.next();
            if (word.matches("(W|w)(O|o)(R|r)(L|l)(D|d)")) result++;
        }
        scanner.close();
        return result;
    }

    private static File getAddressOfFile() throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        reader.close();
        return file;
    }
}
