package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        File inputFile = new File(reader.readLine());
        File outputFile = new File(reader.readLine());
        reader.close();

        WriteAllValues(getAllNumerals(inputFile), outputFile);

    }

    private static ArrayList getAllNumerals(File file) throws IOException
    {
        ArrayList <String> list = new ArrayList<>();

        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("\\s|\\n");

        while (scanner.hasNext())
        {
            String word = scanner.next();
            if (word.matches("\\d+$")) {
                list.add(word);
            if (scanner.hasNext()) list.add(" ");
            }

        }
        scanner.close();
        return list;
    }

    private static void WriteAllValues(ArrayList list, File file) throws IOException
    {

        PrintWriter printWriter = new PrintWriter(file);

        for (Object aList : list)
        {
            printWriter.print(aList);
        }
        printWriter.close();
    }
}
