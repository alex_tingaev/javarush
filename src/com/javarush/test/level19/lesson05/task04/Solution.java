package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        File inputFile = new File(reader.readLine());
        File outputFile = new File(reader.readLine());
        reader.close();

        Scanner scanner = new Scanner(inputFile);
        PrintWriter printWriter = new PrintWriter(outputFile);

        while (scanner.hasNext())
        {
            String buffer = scanner.nextLine().replaceAll("\\.", "!");
            printWriter.println(buffer);
        }
        scanner.close();
        printWriter.close();

    }
}
