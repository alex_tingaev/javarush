package com.javarush.test.level19.lesson05.task01;

/* Четные байты
Считать с консоли 2 имени файла.
Вывести во второй файл все байты с четным индексом.
Пример: второй байт, четвертый байт, шестой байт и т.д.
Закрыть потоки ввода-вывода.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file1 = new File(reader.readLine());
        File file2 = new File(reader.readLine());
        reader.close();
        writeEvenByte(file1, file2);

    }

    public static void writeEvenByte(File file1, File file2) throws IOException{
        FileInputStream fileInputStream = new FileInputStream(file1);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

        while (bufferedInputStream.available() > 0) {
            int buffer;
            buffer = bufferedInputStream.read();
            buffer = bufferedInputStream.read();
            bufferedOutputStream.write(buffer);

        }
        bufferedInputStream.close();
        bufferedOutputStream.close();
    }
}
