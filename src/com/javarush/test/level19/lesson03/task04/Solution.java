package com.javarush.test.level19.lesson03.task04;

import java.io.IOException;
import java.util.*;

/* И еще один адаптер
Адаптировать Scanner к PersonScanner.
Классом-адаптером является PersonScannerAdapter.
Данные в файле хранятся в следующем виде:
Иванов Иван Иванович 31 12 1950

В файле хранится большое количество людей, данные одного человека находятся в одной строке. Метод read() должен читать данные одного человека.
*/

public class Solution {
    public static class PersonScannerAdapter implements PersonScanner {
        Scanner scanner;

        public PersonScannerAdapter(Scanner scanner)
        {
            this.scanner = scanner;
        }


        @Override
        public Person read() throws IOException
        {

            String[] buffer = scanner.nextLine().split(" ");
            String firstName = buffer[1];
            String middleName = buffer[2];
            String lastName = buffer[0];
            Calendar date = new GregorianCalendar(Integer.parseInt(buffer[5]), Integer.parseInt(buffer[4])-1 , Integer.parseInt(buffer[3]));

            return new Person(firstName, middleName, lastName, date.getTime());
        }

        @Override
        public void close() throws IOException
        {
            scanner.close();
        }
    }
}
