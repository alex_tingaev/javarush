package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
Метод main не участвует в тестировании.
*/
public class Solution {
    public static class A {
        protected String name = "A";

        public A()
        {
        }

        public A(String name) {
            this.name += name;
        }
    }

    public class B extends A implements Serializable {
        public B(String name) {
            super(name);
            this.name += name;
        }
    }

    public static void main(String[] args) throws IOException
    {
        Solution solution = new Solution();
        Solution.B b = solution.new B("B");
        System.out.println(b.name);

        FileOutputStream fos = new FileOutputStream(new File("/home/artes/Desktop/file1"));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(b);
        oos.flush();
        oos.close();
    }
}
