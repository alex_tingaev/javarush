package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution
{
    public static void main(String[] args)
    {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try
        {
            File your_file_name = File.createTempFile("your_file_name", null, new File("/home/artes/Desktop"));
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            //User user1 = new User();
            User user2 = new User();

           //user1.setBirthDate(new Date(1988 - 1900, 2, 13));
           //user1.setCountry(User.Country.RUSSIA);
           //user1.setFirstName("Alex");
           //user1.setLastName("Tingaev");
           //user1.setMale(true);

            user2.setBirthDate(null);
            user2.setCountry(User.Country.UKRAINE);
            user2.setFirstName("Ivan");
            user2.setLastName("Panteleev");
            user2.setMale(true);

           // javaRush.users.add(0, user1);
            javaRush.users.add(0, user2);

            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

            for (User u : loadedObject.users) {
                System.out.println(u.getLastName());
                System.out.println(u.getFirstName());
                System.out.println(u.getCountry());
              //  System.out.println(simpleDateFormat.format(u.getBirthDate()));
                System.out.println(u.isMale());
            }

        }
        catch (IOException e)
        {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush
    {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception
        {
            //implement this method - реализуйте этот метод
            PrintWriter printWriter = new PrintWriter(outputStream);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            for (int i = 0; i < users.size(); i++)
            {
                printWriter.println((users.get(i).getBirthDate() != null) ? simpleDateFormat.format(users.get(i).getBirthDate()): "<<BirthDate not allowed>>");
                printWriter.println((users.get(i).getCountry() != null) ? users.get(i).getCountry() : "<<Country not allowed>>");
                printWriter.println((users.get(i).getFirstName() != null) ? users.get(i).getFirstName() : "<<FirstName not allowed>>");
                printWriter.println((users.get(i).getLastName() != null) ? users.get(i).getLastName() : "<<LastName not allowed>>");
                printWriter.println((users.get(i).isMale()) ? "true" : "false");

            }
            printWriter.flush();
            printWriter.close();
        }

        public void load(InputStream inputStream) throws Exception
        {
            //implement this method - реализуйте этот метод


            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            try
            {
                int usersSize = users.size();

                while (reader.ready()) {
                    User user = new User();

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

                    try {Date date = simpleDateFormat.parse(reader.readLine());
                        user.setBirthDate(date);}
                    catch (Exception e) {System.out.println("<<BirthDate not allowed>>");
                        user.setBirthDate(null);}

                    try {user.setCountry(User.Country.valueOf(reader.readLine()));}
                    catch (Exception e)
                    {System.out.println("<<Country not allowed>>");
                        user.setCountry(User.Country.OTHER);}

                    try {user.setFirstName(reader.readLine());}
                    catch (Exception e) {System.out.println("<<FirstName not allowed>>");
                        user.setFirstName(null);}

                    try {user.setLastName(reader.readLine());}
                    catch (Exception e) {System.out.println("<<LastName not allowed>>");
                        user.setLastName(null);}

                    try {user.setMale(Boolean.parseBoolean(reader.readLine()));}
                    catch (Exception e) {System.out.println("?????");
                        user.setMale(false);}
                    users.add(user);
            }
        }
            finally
            {   try
            {
                reader.close();
            }
             catch (IOException e){}
            }
            }
    }
}
